public class Solution {
    int solution(int[] array){

        int b = 1;
        for(int i=0;i<array.length;i++){
            b= b * array[i];
        }

        if(b>0){return 1;}
        else if(b<0){return -1;}
        else{return 0;}
    }
}
